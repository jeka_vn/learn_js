'use strict';

var gulp = require('gulp');
var sass = require('gulp-sass');
var rigger = require('gulp-rigger');
var notify = require('gulp-notify');
var browserSync = require('browser-sync').create();
var autoprefixer = require('gulp-autoprefixer');
var csso = require('gulp-csso');
var babel = require('gulp-babel');
var concat = require('gulp-concat');
var uglify = require('gulp-uglify');
var headerfooter = require('gulp-headerfooter');
var arrJsFile = [
    'src/js/selectAtm.js',
    'src/js/addAtm.js',
    'src/js/initAtm.js',
    'src/js/showListAtm.js',
    'src/js/main.js',
    'src/js/reg_user.js',
    'src/js/remindPin.js',
    'src/js/showCard.js',
    'src/js/popup.js',
    'src/js/check_reg.js'
];


gulp.task('sass', function () {
    return gulp.src('src/scss/style.scss')
        .pipe( sass().on( 'error', notify.onError(
            {
                message: "<%= error.message %>",
                title  : "Ошибка SASS!"
            }))
        )
        .pipe(autoprefixer({
            browsers: ['last 2 versions'],
            cascade: false
        }))
        .pipe(csso())
        .pipe(gulp.dest('dist'));
});

gulp.task('html', function (done) {
    gulp.src('src/html/*.html')
        .pipe(rigger())
        .pipe(gulp.dest('dist/'));
    done();
});

gulp.task('browser-sync', function() {
    browserSync.init({
        server: {
            baseDir: "dist/"
        },
        open: false,
        notify: false
    });
    gulp.watch(["dist/**/*.*"]).on('change', browserSync.reload);
});

gulp.task('js', function () {
    return gulp.src(arrJsFile)
        .pipe(babel({
            presets: ['@babel/env']
        }))
        .pipe(concat('scripts.js'))
        .pipe(headerfooter.header('(function(){'))
        .pipe(headerfooter.footer('}());'))
        .pipe(uglify())
        .pipe(gulp.dest("dist/js"));
});

gulp.task('img', function () {
    return gulp.src('src/img/*.*')
        .pipe(gulp.dest('dist/img'));
});


gulp.task('watch', function() {
    gulp.watch('src/scss/**/*.scss', gulp.series('sass'));
    gulp.watch('src/html/**/*.html', gulp.series('html'));
    gulp.watch('src/js/*.js', gulp.series('js'));
});
gulp.task('default', gulp.series(
    gulp.parallel('watch', 'browser-sync', 'html', 'js', 'sass', 'img')
));
