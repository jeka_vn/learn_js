// Функция получает данные карты и выводит номер и срок действия.
function showCard() {
    let showCardEl = document.querySelector('#showCard');
    let cardNumEl = showCardEl.querySelector('.cardNum');
    let endDateEl = showCardEl.querySelector('.endDate');

    let cardData = JSON.parse(localStorage.getItem('cardData'));
    let {cardNum, endDate} = cardData;

    let newCardNum;
    let count = 0;
    for (let i = 0; i < cardNum.length; i++) {
        count++;

        (newCardNum === undefined) ? newCardNum = cardNum[i] : newCardNum += cardNum[i];


        if (count === 4) {
            newCardNum += ' ';
            count = 0;
        }
    }

    cardNumEl.textContent = newCardNum;
    endDateEl.textContent = endDate;
}
if (checkReg() === true) {
    showCard();
}
