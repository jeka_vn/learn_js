'use strict';

let atmKeyboard = document.querySelector('.atm-keyboard');

function Atm() {
    let showScreen = {
        pin: true,
        welcome: false,
        balance: false,
        phone: false,
        cash: false,
        confirm: false,
        exit: false
    };


    let userPin = undefined;

    let withAmount;
    let num = 0;

    let newPhone = '';

    this.input = (keyNum) => {
        state(keyNum);
    };
    this.actionBtn = (keyCommand) => {
        state(keyCommand);
    };
    this.getData = (data) => {
        showHideScreen();
        state();
        // return getUserData(data);
    };
    // private
    function state(key) {
        let userCash = getUserData('cash');

        if (showScreen.pin === true) {
            pin(key);
        } else if (showScreen.welcome === true) {
            welcome(key);
        } else if (showScreen.phone === true) {
            changePhone(key);
        } else if (showScreen.balance === true) {
            checkBalanse(key);
        } else if (showScreen.cash === true) {
            cash(key);
        } else if (showScreen.confirm === true) {
            confirm(key);
        } else if (showScreen.exit === true) {
            exit(key);
        }

        // Логика екрана ввода пин кода
        function pin(key) {

            if ((key === 'enter') && (userPin.length === 4)) {
                checkPin();
            }

            if ((key !== 'clear') && (key !== 'cansel') && (key !== 'enter')) {
                if (userPin !== undefined) {
                    (userPin.length !== 4) ? userPin += key : '';
                    showPin('show');

                } else if (userPin === undefined) {
                    userPin = key;
                    if (userPin !== undefined) showPin('show');
                }

            } else if (key === 'clear') {
                showPin('hide');
            }

            // show pin
            function showPin(flag) {
                let fieldPin = document.querySelector('#fieldPin');
                let symbols;

                if (flag === 'show') {
                    console.log(userPin);
                    for (let i = 0; i < userPin.length; i++) {
                        if (symbols === undefined) {
                            symbols = '*';
                            fieldPin.textContent = symbols;
                        } else {
                            symbols += '*';
                            fieldPin.textContent = symbols;
                        }
                    }
                } else if (flag === 'hide') {
                    userPin = undefined;
                    fieldPin.textContent = '';
                }
            }

            // Проверка пин-кода
            function checkPin() {
                let pinCode = getUserData('pinCode');

                userPin = +userPin;
                pinCode = +pinCode;

                if (userPin === pinCode) {
                    showScreen.pin = false;
                    showScreen.welcome = true;
                    state();
                    showPin('hide');


                } else {
                    showPin('hide');
                    showHideMsg('Банк', 'Пин-код неверный', 'tomato');
                }
            }
        }
        // Логика екрана приветствия
        function welcome(key) {
            let {name, patronymic} = getUserData('fullName');

            let welcomeScreen = document.querySelector('#welcomeScreen');
            let userName = welcomeScreen.querySelector('.user-name');

            userName.textContent = `${name} ${patronymic}`;


            welcomeScreen.onclick = (event) => {
                let eventClass = event.target.className;
                switch (eventClass) {
                    case 'change-phone':
                        showScreen.welcome = false;
                        showScreen.phone = true;
                        state();
                        break;
                    case 'check-balance':
                        showScreen.welcome = false;
                        showScreen.balance = true;
                        state();
                        break;
                    case 'cash':
                        showScreen.welcome = false;
                        showScreen.cash = true;
                        state();
                        break;
                }
            };

            if (key === 'cansel') {
                showScreen.pin = true;
                showScreen.welcome = false;
                state();
            }
        }
        // Изменение номера

        function changePhone(key) {
            let userPhone = getUserData('phone');
            let phoneScreen = document.querySelector('#phoneScreen');
            let newPhoneNum = phoneScreen.querySelector('#newPhoneNum');
            newPhoneNum.textContent = `${userPhone}`;

            if (!isNaN(+key)) {

                if ((newPhone.length <= 9) && (newPhone.length !== 10)) {
                    newPhone += key;
                    newPhoneNum.textContent = `${newPhone}`;

                } else  {
                    newPhoneNum.textContent = `${newPhone}`;
                }

            }
            if ((newPhone.length === 10) && (key === 'enter')) {
                let userData = getUserData('userData');
                userData.phone = newPhone;

               localStorage.setItem('userData', JSON.stringify(userData));

               showHideMsg('Банк','Ваш номер изменен', 'green');
            }

            if (key === 'clear') {
                newPhone = '';
            }

            if (key === 'cansel') {
                showScreen.welcome = true;
                showScreen.phone = false;
                state();
            }
        }
        // проверка баланса
        function checkBalanse() {
            let userCash = getUserData('cash');
            let balanceScreen = document.querySelector('#balanceScreen');
            let moneyEl = balanceScreen.querySelector('.money');

            moneyEl.textContent = `${userCash}`;

            if (key === 'cansel') {
                showScreen.welcome = true;
                showScreen.balance = false;
                state();
            }
        }
        // Логика екрана ввода суммы снятия
        function cash(key) {

            let cashScreen = document.querySelector('#cashScreen');
            let money = cashScreen.querySelector('.money');
            money.textContent = `${userCash}`;

            let inputCash = cashScreen.querySelector('.input-cash');

            cashScreen.onclick = (event) => {
                let eventClass = event.target.className;

                if (eventClass === 'currency-size') {

                    let moneyAmount = +event.target.textContent;

                    if (num < getUserData('cash')) {

                        if (withAmount === undefined) {
                            userCash -= (withAmount = moneyAmount);
                            num = moneyAmount;
                        } else {
                            userCash -= (withAmount = moneyAmount);
                            num += moneyAmount;
                        }

                        money.textContent = `${userCash}`;
                        inputCash.textContent = `${num}`;

                        if (num > getUserData('cash')) {
                            userCash = 0;
                            num = +getUserData('cash');
                            money.textContent = `${userCash}`;
                            inputCash.textContent = `${num}`;
                        }
                    }
                }
            };

            if (key === 'enter') {
                showScreen.cash = false;
                showScreen.confirm = true;
                state();
            } else if (key === 'cansel') {
                showScreen.welcome = true;
                showScreen.cash = false;
            }
        }
        // Логика екрана подтвержденя снятия
        function confirm(key) {
            let cardData = getUserData('cardData');

            let confirmScreen = document.querySelector('#confirmScreen');
            let amountMoney = confirmScreen.querySelector('.amount-money');
            let commissionEl = confirmScreen.querySelector('#commission');
            amountMoney.textContent = `${num}`;

            let selectedAtm = localStorage.getItem('idSelectedAtm');
            let arrListAtm = JSON.parse(localStorage.getItem('listAtm'));
            let commission = null;


            arrListAtm.forEach((item) => {
                if (item.idAtm === selectedAtm) commission = item.commission;
            });
            let percent = num / 100 * commission;
            commissionEl.textContent = `${percent}`;



            if (key === 'enter') {
                cardData.cash -= (num + percent);
                localStorage.setItem('cardData', JSON.stringify(cardData));
                showScreen.confirm = false;
                showScreen.exit = true;
                state();
                showHideMsg('Банк', `с Вашего счета снято ${(num + percent)}грн.`, 'green')
            } else if (key === 'cansel') {
                showScreen.cash = true;
                showScreen.confirm = false;
            }
        }
        // Логика екрана выхода
        function exit(key) {
            let payTheMoney = document.querySelector('#payTheMoney');
            // let moneyIssue = payTheMoney.querySelector('.money-issue');
            // moneyIssue.style.display = 'block';

            // if (num >= 50) {
            //     console.log('50');
            //     moneyIssue.classList.add('money50');
            // } else if (num >= 100) {
            //     console.log('100');
            //     moneyIssue.classList.add('money100');
            // } else if (num >= 200) {
            //     console.log('200');
            //     moneyIssue.classList.add('money200');
            // }

            if (key === 'enter') {
                showScreen.exit = false;
                showScreen.pin = true;
                // moneyIssue.style.display = 'none';

                userPin = undefined;
                console.log(userPin);
                withAmount = 0;
                num = 0;
            }
        }

        showHideScreen();
    }

    function showHideScreen() {
        let doc = document;
        let screen = {
            pin: doc.querySelector('#pinScreen'),
            welcome: doc.querySelector('#welcomeScreen'),
            balance: doc.querySelector('#balanceScreen'),
            phone: doc.querySelector('#phoneScreen'),
            cash: doc.querySelector('#cashScreen'),
            confirm: doc.querySelector('#confirmScreen'),
            exit: doc.querySelector('#exitScreen')
        };

        for (let nameScreen in showScreen) {
            if (showScreen.hasOwnProperty(nameScreen)) {
                if (showScreen[nameScreen] === true) {

                    for (let keys in screen) {
                        if (screen.hasOwnProperty(keys)) {
                            if (nameScreen === keys) {
                                screen[keys].style.display = 'block';
                            } else {
                                screen[keys].style.display = 'none';
                            }

                        }
                    }

                }

            }
        }

    }

    // showMsg
    function showHideMsg(title, text, color) {
        let msgBox = document.querySelector('#msgBox');
        let msg = msgBox.querySelector('.msg');
        let titleMsg = msgBox.querySelector('.title');
        let textMsg = msgBox.querySelector('.text');

        msg.style.background = color;
        titleMsg.textContent = title;
        textMsg.textContent = text;
        msg.style.left = '0px';

        setTimeout(() => {
            msg.style.left = '250px';
        },2500);
    }

// Получение данных из localstorage
    function getUserData(request) {
        let userData = JSON.parse(localStorage.getItem('userData'));
        let cardData = JSON.parse(localStorage.getItem('cardData'));

        switch (request) {
            case 'all':
                return { userData, cardData };
            case 'pinCode':
                return cardData.pin;
            case 'fullName':
                return userData;
            case 'cash':
                return cardData.cash;
            case 'cardData':
                return cardData;
            case 'phone':
                return userData.phone;
            case 'userData':
                return userData;
        }
    }
}

let atm = new Atm();

atmKeyboard.onclick = (event) => {
    let classEvent = event.target.className;
    let eventContent;
    // console.log(event.target.textContent);

    switch (classEvent) {
        case 'num':
            eventContent = event.target.textContent;
            atm.input(eventContent);
            // console.log('num');
            break;
        case 'clear':
        case 'cansel':
        case 'enter':
            atm.actionBtn(classEvent);
            break;
    }

};
