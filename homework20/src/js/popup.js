'use strict';

// Функция pop-up. вызываеться при регистрации пользователя, регистрации банкомата,
// показа списка банкоматов, показа пин-кода и показе карты
function popUp (flag, instruction) {
    let popUpBox = document.querySelector('#popUpBox');
    let atmList = popUpBox.querySelector('#atmList');
    let regUser = popUpBox.querySelector('#regUser');
    let regAtm = popUpBox.querySelector('#regAtm');
    let remindPinCode = popUpBox.querySelector('#remindPinCode');
    let showCard = popUpBox.querySelector('#showCard');
    let listAtm = popUpBox.querySelector('#atmList');

    switch (instruction) {
        case 'regUser':
            regUser.style.display = 'block';
            break;
        case 'regAtm':
            regAtm.style.display = 'block';
            break;
        case 'atmList':
            listAtm.style.display = 'block';
            break;
        case 'pinCode':
            remindPinCode.style.display = 'block';
            break;
        case 'showCard':
            showCard.style.display = 'block';
            break;
    }

    switch (flag) {
        case 'show':
            popUpBox.style.display = 'flex';

            let show = setTimeout(() => {
                popUpBox.style.opacity = '1';
                clearTimeout(show);
            }, 10);

            break;
        case 'hide':
            let hide = setTimeout(() => {
                popUpBox.style.display = 'none';
                clearTimeout(hide);
            }, 500);

            popUpBox.style.opacity = '0';

            regUser.style.display = 'none';
            regAtm.style.display = 'none';
            listAtm.style.display = 'none';
            remindPinCode.style.display = 'none';
            showCard.style.display = 'none';
            // remindPinCode.innerHTML = '';
            atmList.innerHTML = '';
            break;
    }
}


let docEl = document.querySelector('#doc');
docEl.onclick = (event) => {
    let eventClass = event.target.className;

    switch (eventClass) {
        case 'regAtm':
            popUp('show', 'regAtm');
            break;
        case 'remindPin':
            remindPinCode();
            popUp('show', 'pinCode');
            break;
        case 'selectAtm':
            showListAtm();
            popUp('show', 'atmList');
            break;
        case 'showUserCard':
            showCard();
            popUp('show', 'showCard');
            break;
        case 'pop_up-box':
            popUp('hide');
            break;
    }
};
