'use strict';
/*
* Функция проверяет зарегистрирован ли пользователь
* return true or false
* */
function checkReg() {
    let userData = localStorage.getItem('userData');
    let popUpBox = document.querySelector('#popUpBox');

    if (userData === null) {
        popUpBox.classList.add('reg');
        popUp('show', 'regUser');
        return false;
    } else {
        popUpBox.classList.remove('reg');
        return true;
    }
}

checkReg();
