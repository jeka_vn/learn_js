'use strict';
// Функция проверяет если ли в localstorage список банкоматов
// и если нету - добавляет
let listAtm = localStorage.getItem('listAtm');

if (listAtm === null) {
    let listAtm = [
        {
            city: "Одесса",
            street: "Заболотного",
            houseNum: "4",
            commission: 2,
            idAtm: "489900"
        },
        {
            city: "Киев",
            street: "Юности",
            houseNum: "127",
            commission: 4,
            idAtm: "467700"
        }
    ];
    localStorage.setItem('listAtm', JSON.stringify(listAtm));
}
