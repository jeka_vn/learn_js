'use strict';
// Функция выбора банкомата.
// При клике на кнопку 'выбрать' получает ид банкомата и записывает
// выбраный банкомат в localstorage
	function selectAtm (id) {
		let idToNum = +id;
		let listAtm = JSON.parse(localStorage.getItem('listAtm'));


		for (let i = 0; i < listAtm.length; i++) {
			let idAtm = +listAtm[i].idAtm;


			if (idAtm === idToNum) {
				let numArr = i;
				localStorage.setItem('idSelectedAtm', id);
				popUp('hide');
				atmDisplay(idAtm, numArr);
				break;
			}

		}
		// Функция отображает адресс выбраного банкомата
		function atmDisplay(idAtm, numArr) {
			// listAtm[numArr].city;
			// console.log(listAtm[numArr].city);
			let location = document.querySelector('#localion');
			let atmTitle = document.querySelector('#atmTitle');
			let atmBox = document.querySelector('.atm-box');

			location.textContent = `${listAtm[numArr].city}, Ул.${listAtm[numArr].street}, ${listAtm[numArr].houseNum}`;
			atmTitle.style.display = 'none';
			atmBox.style.display = 'block';
		}

	}
	let atmListEl = document.querySelector('#atmList');
	let idSelectedAtm = localStorage.getItem('idSelectedAtm');
	atmListEl.onclick = (event) => {
		let idBtn = event.target.id;
		let eventClass = event.target.className;

		if (eventClass === 'atmBtn') {
			selectAtm(idBtn);
		}
	};

	if (idSelectedAtm !== null) {
		selectAtm(idSelectedAtm);

	}
