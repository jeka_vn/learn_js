'use strict';
// добавление нового банкомата
    let regAtm = document.querySelector('#regAtm');

    let addAtnBtn = document.querySelector('#addAtnBtn');

    function addAtm() {
        // input
        let cityInpt = regAtm.querySelector('#city').value;
        let streetInpt = regAtm.querySelector('#street').value;
        let houseNumInpt = regAtm.querySelector('#houseNum').value;

        // Если функция валидации вернула true - записываем данные в localstorage
        if (validate() !== true) {
            let listAtm = JSON.parse(localStorage.getItem('listAtm'));
            let atmData = {
                city: cityInpt,
                street: streetInpt,
                houseNum: houseNumInpt,
                idAtm: Math.random().toString().slice(4, 8),
                commission: Math.round(Math.random() * 5)
            };

            listAtm.push(atmData);
            localStorage.setItem('listAtm', JSON.stringify(listAtm));
            popUp('hide');
        }

        // Валидация полей
        function validate () {
            // error input
            let cityError = regAtm.querySelector('#cityError');
            let streetError = regAtm.querySelector('#streetError');
            let houseNumError = regAtm.querySelector('#houseNumError');


            if (cityInpt === '') {
                cityError.style.display = 'block';
                cityError.textContent = 'Вы не указали город';
                return true;
            } else {
                cityError.style.display = 'none';
            }
            if (streetInpt === '') {
                streetError.style.display = 'block';
                streetError.textContent = 'Вы не указали улицу';
                return true;

            } else {
                streetError.style.display = 'none';
            }
            if (houseNumInpt === '') {
                houseNumError.style.display = 'block';
                houseNumError.textContent = 'Вы не указали номер дома'
                return true;

            } else {
                houseNumError.style.display = 'none';
            }

            return false;
        }
    }


    addAtnBtn.onclick = () => {
        addAtm();
    };
