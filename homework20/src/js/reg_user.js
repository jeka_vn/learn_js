'use strict';
    // Регистрация пользователя
    let regUser = document.querySelector('#regUser');

    let regBtn = regUser.querySelector('#regBtn');

    function addUser () {
        // input
        let nameInpt = regUser.querySelector('#firsName').value;
        let patronymicInpt = regUser.querySelector('#patronymic').value;
        let surnameInpt = regUser.querySelector('#surname').value;
        let phoneNumInpt = regUser.querySelector('#phoneNum').value;


        if (validate() !== true) {
            let userData = {
                name: nameInpt,
                patronymic: patronymicInpt,
                surname: surnameInpt,
                phone: phoneNumInpt
            };

            localStorage.setItem('userData', JSON.stringify(userData));
            createCardDara();
            popUp('hide');
            checkReg();
        }

        // Валидация полей
        function validate () {
            // error input
            let firsNameError = regUser.querySelector('#firsNameError');
            let patronymicError = regUser.querySelector('#patronymicError');
            let surnameError = regUser.querySelector('#surnameError');
            let phoneNumError = regUser.querySelector('#phoneNumError');


            if (nameInpt === '') {
                firsNameError.style.display = 'block';
                firsNameError.textContent = 'Вы не указали имя';
                return true;
            } else {
                firsNameError.style.display = 'none';
            }
            if (patronymicInpt === '') {
                patronymicError.style.display = 'block';
                patronymicError.textContent = 'Вы не указали отчество';
                return true;

            } else {
                patronymicError.style.display = 'none';
            }
            if (patronymicInpt === '') {
                patronymicError.style.display = 'block';
                patronymicError.textContent = 'Вы не указали отчество';
                return true;

            } else {
                patronymicError.style.display = 'none';
            }
            if (surnameInpt === '') {
                surnameError.style.display = 'block';
                surnameError.textContent = 'Вы не указали фамилию';
                return true;

            } else {
                surnameError.style.display = 'none';
            }
            if (phoneNumInpt.length !== 10) {
                phoneNumError.style.display = 'block';
                phoneNumError.textContent = 'Номер должен состоять из 10 цыфр';
                return true;
            } else {
                phoneNumError.style.display = 'none';
            }
            return false;
        }

        // Генирация данных карты
        function createCardDara() {
            let endYear = new Date().getFullYear() + 3;
            endYear = endYear.toString();
            if (endYear.length === 1) endYear = '0' + endYear;

            let endMonth = new Date().getMonth() + 1;
            endMonth = endMonth.toString();
            if (endMonth.length === 1) endMonth = '0' + endMonth;


            let cardData = {
                cardNum: Math.random().toString().slice(2, 18),
                endDate: `${endYear}/${endMonth}`,
                pin: Math.random().toString().slice(4, 8),
                cash: 11000
            };

            localStorage.setItem('cardData', JSON.stringify(cardData));
        }
    }


    regBtn.onclick = () => {
        addUser();
    };
