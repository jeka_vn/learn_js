// Функция паказа пин-кода. Получает пин из localstorage и добавляет его
// в елемент на страницу
function remindPinCode() {
    let remindPinCodeEl = document.querySelector('#remindPinCode');
    let h2 = remindPinCodeEl.querySelector('h2');
    let span = h2.querySelector('span');
    span.textContent = JSON.parse(localStorage.getItem('cardData')).pin;
}
