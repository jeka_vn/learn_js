'use strict';
// Функция отображает список банкоматов
function showListAtm() {
    let atmListEl = document.querySelector('#atmList');
    let listAtm = JSON.parse(localStorage.getItem('listAtm'));

    listAtm.forEach((item) => {
        let itemAtm = document.createElement('div');
        itemAtm.classList.add('item_atm');
        itemAtm.id = item.idAtm;

        itemAtm.innerHTML = `
        <p>Город: ${item.city}</p>
        <p>Улица: ${item.street}</p>
        <p>Номер дома: ${item.houseNum}</p>
        <p>Комиссия: ${item.commission}%</p>
        <button class="atmBtn" id=${item.idAtm}>Выбрать</button>
        `;

        atmListEl.appendChild(itemAtm);
    })
}
