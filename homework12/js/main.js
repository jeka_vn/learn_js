'use strict';

var users = {};

var cardBox = document.querySelector('#cardBox');

var formButton = document.querySelector('#formBtn');

function User() {
    var userId;
    var userData = {};

    return {
        /*
        * Метод принимает ФИО {string}, email{string}, id{number]
        * Польное имя передаеться в функцию parseFulName()
        * */
        getUserData: function (fullName, email, id) {
            parseFulName(fullName);
            userData.userEmail = email;
            userId = id;
        },
        /*
        * Метод вызывает функцию createCard(), которая создает карту пользователя
        * */
        showCard: function () {
            /*
             * Функция создния новой карточки пользователя.
             * Принимает {object} и userId {number}
             * Создает разметку html и записывает туда данные пользователя.
            */
            createCard(userData, userId);
        },
        /*
        * Метод очищает обьект с данными пользователя
        * */
        removeUser: function () {
            return userData = {};
        },
        /*
        *  Метод очищает данные пользователя из html которую пользователь
        *  выбрал для редактирования
        */
        editData: function (userId) {
            var card = document.querySelector('#' + userId);
            var btnBox = card.querySelector('.btnBox');
            btnBox.style.display = 'none';
            card.querySelector('.fullNameEl').textContent = '';
            card.querySelector('.cardEmail').textContent = '';

            card.appendChild(createForm(userData));
        },
        /*
         * Метод изменения данных карточки после редактирования.
         */
        changeData: function () {
            var card = document.querySelector('#' + userId);
            var btnBoxEl = card.querySelector('.btnBox');
            var fullNameEl = card.querySelector('.fullNameEl');
            var emailEl = card.querySelector('.cardEmail');
            var changeFormEl = card.querySelector('#changeForm');

            var fullName;
            var email;

            for (var key in userData) {
                if ((userData.hasOwnProperty(key))) {
                    if (!isNaN(+key)) {
                        (typeof  fullName === 'undefined') ? fullName = userData[key] + ' ' : fullName += userData[key] + ' ';
                    } else  {
                        email = userData[key];
                    }
                }
            }
            changeFormEl.remove();
            fullNameEl.textContent = fullName;
            emailEl.textContent = email;
            btnBoxEl.style.display = 'block';
        }
    };
    /*
    * Функция рандомно генерирует число от 1 до 5.
    * Згенерированое число подставляеться к пути изображения и возвращает путь к изображению
    * */
    function randonBg() {
        var ramdonNum = Math.round(Math.random() * 5);
        return './img/' + ramdonNum + '.jpg';
    }


    /*
    * Функция создает форму для редакторования карточки.
    * Принимает {object}
    * */
    function createForm(userData) {

        var fullName;
        var email;

        for (var key in userData) {
            if ((userData.hasOwnProperty(key))) {
                if (!isNaN(+key)) {
                    (typeof  fullName === 'undefined') ? fullName = userData[key] + ' ' : fullName += userData[key] + ' ';
                } else  {
                    email = userData[key];
                }
            }
        }


        var containerForm = document.createElement('div');
        containerForm.id = 'changeForm';

        var fullNameInput = document.createElement('input');
        fullNameInput.type = 'text';
        fullNameInput.classList.add('fullNameInput');
        fullNameInput.id = 'fullNameInput';
        fullNameInput.setAttribute('value', fullName);

        var emailInput = document.createElement('input');
        emailInput.type = 'text';
        emailInput.classList.add('emailInput');
        emailInput.id = 'emailInput';
        emailInput.setAttribute('value',email);

        var btn = document.createElement('button');
        btn.classList.add('changeBtn');
        btn.id = 'changeBtn';
        btn.textContent = 'Изменить';

        containerForm.appendChild(fullNameInput);
        containerForm.appendChild(emailInput);
        containerForm.appendChild(btn);

        return containerForm;
    }

    /*
    * Функция создния новой карточки пользователя.
    * Принимает {object} и userId {number}
    * Создает разметку html и записывает туда данные пользователя.
    * */
    function createCard(userData, userId) {
        var cardContainer = document.querySelector('#cardBox');
        var createBoxCard = document.createElement('div');
        createBoxCard.classList.add('card');
        createBoxCard.id = userId;
        createBoxCard.style.backgroundImage = 'url('+ randonBg() +')';

        var fullNameEl = document.createElement('div');
        fullNameEl.classList.add('fullNameEl');

        var emailEl = document.createElement('div');
        emailEl.classList.add('cardEmail');

        emailEl.textContent = userData.userEmail;

        for (var key in userData) {
            if ((typeof +key === 'number') && (!isNaN(+key))) {
                if (userData.hasOwnProperty(key)) {
                    fullNameEl.textContent += userData[key] + ' ';
                }

            }
        }

        createBoxCard.appendChild(generateBtn());
        createBoxCard.appendChild(fullNameEl);
        createBoxCard.appendChild(emailEl);
        cardContainer.appendChild(createBoxCard);

        function generateBtn() {
            var btnBox = document.createElement('div');
            btnBox.classList.add('btnBox');

            var delBtn = document.createElement('span');
            delBtn.classList.add('delBtn');
            delBtn.textContent = 'Удалить';

            var editBtn = document.createElement('span');
            editBtn.classList.add('editBtn');
            editBtn.textContent = 'Изменить';

            btnBox.appendChild(editBtn);
            btnBox.appendChild(delBtn);

            return btnBox;
        }
    }

    /*
    * Функция разбивает ФИО на массив и записывает данные в обьект userData
    * */
    function parseFulName(fullName) {
        var arrFullName = fullName.split(' ');

        arrFullName.forEach(function (item, index) {
            userData[index] = item;
        });
    }


}

/*
* Функция генерации уникального имени
* return {string}
* */
function getHash() {
    var date = new Date();
    var hash = date.getTime();

    return 'user' + hash;
}

formButton.onclick = function (event) {
    event.preventDefault();

    var hash = getHash();

    var fullName = document.querySelector('#fullName');
    var email = document.querySelector('#email');
    var fullNameVal = fullName.value;
    var emailVal = email.value;

    users[hash] = User();
    users[hash].getUserData(fullNameVal, emailVal, hash);
    users[hash].showCard();
};

cardBox.onclick = function (event) {
    var eventClassName = event.target.className;
    var id = null;

    switch (eventClassName) {
        case 'delBtn':
            id = event.target.parentElement.parentElement.id;
            var elem = document.querySelector('#' + id);
            elem.remove();
            users[id].removeUser();
            break;
        case 'editBtn':
            id = event.target.parentElement.parentElement.id;
            users[id].editData(id);
            break;
        case 'changeBtn':
            id = event.target.parentElement.parentElement.id;
            var fullNameInput = document.querySelector('#fullNameInput').value;
            var emailInput = document.querySelector('#emailInput').value;
            users[id].getUserData(fullNameInput, emailInput, id);

            users[id].changeData();
            break;

    }
};
