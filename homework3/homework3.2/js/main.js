'use strict';

var luggageWeight = +prompt('Укажте пажалуйста вес вашей ручной клади:');
var rate = 2.20462;
var pricePerWeightInPounds;
var roundingAmount;

if (!isNaN(luggageWeight) && (luggageWeight != '')) {

	pricePerWeightInPounds = luggageWeight * rate;
	roundingAmount = pricePerWeightInPounds.toFixed(1);

	if ((luggageWeight >= 0) && (luggageWeight <= 5)) {
		console.log('С вас 3грн или ' + roundingAmount + ' фунта');
		alert('С вас 3грн');
	} else if ((luggageWeight >= 5) && (luggageWeight <= 10)) {
		console.log('С вас 5грн или ' + roundingAmount + ' фунта');
		alert('С вас 5грн');
	} else if ((luggageWeight >= 10) && (luggageWeight <= 15)) {
		console.log('С вас 10грн или ' + roundingAmount + ' фунта');
		alert('С вас 10грн');
	} else if ((luggageWeight > 15)) {
		console.log('0966473846. Номер дяди Арсена с газелью');
	}
} else if (isNaN(luggageWeight)) {
	console.log('Ошибка! Можно ввести только цыфры.');
} else if (luggageWeight == '') {
	console.log('Вы нечего не ввели или ввели 0');
}
