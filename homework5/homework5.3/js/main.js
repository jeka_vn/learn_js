'use strict';
var products = {};

var showInputResult = true;

while (showInputResult) {
	showInput();
}



// Функция выводит промт-ы и в конце спрашивает, продолжить ввод или закончить
// и в зависимости от результата, записывает true/false для остановки цикла.
// После чего, управление передается функции validate();
function showInput() {
	var index = +prompt('Введите индекс');
	var name = prompt('Название товара');
	var price = +prompt('Цена');
	var cat = prompt('Категория');
	var completeInput = confirm('Продолжить ввод?');

	if (completeInput) {
		showInputResult = true;
	} else {
		showInputResult = false;
	}

	validate(index, name, price, cat);
}

// Функция принимает 4 аргумента: индекс товара, название, цена, категория.
// Проверяет есть ли товар с таким индексом, то присваивает 
// checkKey = false и останавливает цикл(знаю что не логично, но вынашивал идею всего этого кода полтора дня и написал на одном дыхании).
// Если товара с таким индексом нету, то выполняем проверку всех данных и если есть проблемы - выводим сообщение,
// иначе передаем все данные функции readData()
function validate(index, name, price, cat) {
	var checkKey = true;

	for (var key in products) {
		if (key == index) {
			checkKey = false;
			console.log('такой ключ уже существует');
			break;
		}
	}

	if (checkKey) {
		if (isNaN(index)) {
			console.log('не число');
		} else if (name.length == 0) {
			console.log('Вы не ввели название');
		} else if (price == 0) {
			console.log('Некоректная цена');
		} else if (cat.length == 0) {
			console.log('Некоректное название категории');
		} else {
			(checkIndex(index)) ? readData(index, name, price, cat) : console.log('Вы ввели неверный индекс товара');
		}
	}

	function checkIndex(index) {
		if ((index >= 0) && (index <=10)) {
			return true;
		}

		return false;
	}
}

// Функция получает индекс, название продукта, цену и категорию, 
// создает обьект и записывает его в обьект products.
function readData(index, name, price, cat) {
	var obj = {};

	obj.name = name;
	obj.price = price;
	obj.category = cat;

	products[index] = obj;
}

// Функция получает название категории и если найдет такую категорию - выведет новые объекты
// с названием товара и ценами.
function sortProduct(category) {

	for (var item in products) {

		if (products[item].category == category) {
			var item = {
				'name': products[item].name,
				'price': products[item].price,
				// 'category': products[item].category 
			}
			console.log(item);
		}

	}
}


// функция выводит суму цен всех товаров определённой категории. 
// Если сумма превышает 5, то выводится сумма со скидкой в 12%.
function showPrice(category) {

	var price = 0;
	var priceDiscount;
	var discount = 12;
	var count = 0;

	for (var item in products) {

		if (products[item].category == category) {
			price += products[item].price;
			count++;
		}

	}

	if ((count > 5) || (count < 5)) {
		priceDiscount = price / 100 * 12;
		price -= priceDiscount;
		showMsg(discount, price);
	}

}

function showMsg(discount, price) {
	var msg = 'Цена со скиндкой ' + discount + '% ' + 'составит ' + price + ' грн.';
	alert(msg);
}

sortProduct('електро');
showPrice('електро');



// Признаюсь честно. 
// Когда писал код, не ставил коментарии и вернувшись сюда я чуть не сошел с ума))

// п.с. Вернувшись к коду через пару дней сразу увидел что сделал не так, например:  вывести ошибки с validate() через функцию showMsg наверно было бы лучше.