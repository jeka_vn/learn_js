'use strict';
var items = {};

var accordionBox = document.querySelector('#accordionBox');
var popUp = document.querySelector('#popUp');


// Функция принимает инструкцию show/hidden {string}, а также ид елемента,
// по которому кликнул пользователь.
// Получает данные: title, text {object} и вставляет их в pop-up
function showHiddenPopUp(instruction, itemId) {
    var itemData;
    (typeof itemId !== 'undefined') ? itemData = items[itemId].getData() : itemData = {};

    var popUpTitle = popUp.querySelector('#title');
    var popUpText = popUp.querySelector('#text');

    if (instruction === 'show') {
        popUp.style.display = 'block';
        popUpTitle.textContent = itemData.title;
        popUpText.textContent = itemData.text;

    } else if (instruction === 'hidden') {
        popUp.style.display = 'none';
    }

}

function accordion() {
    var itemId = null;
    var itemState = false;
    var itemTitle;
    var itemText;

    return {
        // Получаем id елемента и записываем его в переменную itemId
        addItem: function (id) {
            itemId = id;
        },
        /*
        * Метод в зависимости от состояние itemState изменяет состояние итема акордиона
         */
        changeState: function () {
            if (itemState === false) {
                itemState = true;
                showHidden(true);
            } else if (itemState === true) {
                itemState = false;
                showHidden(false);
            }
        },
        // получаем заголовок и текст итема и записываем их в itemTitle, itemText
        setData: function (title, text) {
            itemTitle = title;
            itemText = text;
        },
        // Возвражаем ибьект с заголовком и текстом итема
        getData: function () {
            return {
                'title': itemTitle,
                'text': itemText
            };
        },
        // возвращаем ид итема
        getId: function () {
            return itemId;
        },
        // Закрываем итем и менняем состояние итема
        close: function () {
            var item = accordionBox.querySelector('#' + itemId);
            var accordion_text = item.querySelector('.accordion_text');
            item.classList.remove('active');
            accordion_text.classList.remove('active');

            itemState = false;
        }
    };

    function showHidden(itemState) {
        var item = accordionBox.querySelector('#' + itemId);
        var accordion_text = item.querySelector('.accordion_text');

        if (itemState === true) {
            item.classList.add('active');
            accordion_text.classList.add('active');
        } else {
            item.classList.remove('active');
            accordion_text.classList.remove('active');
        }
    }
}

accordionBox.onclick = function(event) {
    var itemClass = event.target.classList[0];
    var itemId = event.target.parentElement.id;

    switch (itemClass) {
        case 'accordion_title':
            items[itemId].changeState();
            break;
        case 'accordion_text':
            showHiddenPopUp('show', itemId);
            break;
    }

    for (var key in items) {
        if (items.hasOwnProperty(key)) {
            if (items[key].getId() !== itemId) {
                items[key].close();
            }
        }
    }
};

popUp.onclick = function (event) {
    var classEvent = event.target.className;

    if ((classEvent === 'close_btn') || (classEvent === 'pop-up_container')) {
        showHiddenPopUp('hidden');
    }
};

window.onload = function () {
    var itemEl = accordionBox.querySelectorAll('.item');
    var id;
    var thisItem;
    var titleEl;
    var textEl;

    for (var i = 0; i < itemEl.length; i++) {
        id = 'item' + i;
        itemEl[i].id = id;
        items[id] = accordion();
        items[id].addItem(id);

        thisItem = document.querySelector('#' + id);
        titleEl = thisItem.querySelector('.accordion_title').textContent;
        textEl = thisItem.querySelector('.accordion_text').textContent;

        items[id].setData(titleEl, textEl);
    }
};
