'use strict';
/*
* Функция принимает, часть учебнка{number},
* главу {number or string(название главы)} и вид сортировки {string (current, number, dash)}
* getData(1, 5, 'current') or getData(1, 'Замыкания, область видимости', 'current');
* */
function getData(section, head, output) {
    var sectionCollection = document.querySelectorAll('.frontpage-content__part');
    var sectionLength = sectionCollection.length;
    var collectionThemes = searchAllChapters(section - 1);
    var numHead = 0;
    var arrTopics = searchTopics(collectionThemes, head);
    var count = counter();

    function counter() {
        var count = 1;
        return function () {
            return count++;
        }
    }

    // Поиск всех глав с указаной части учебника
    // Возврашает колекцию глав и тем
    function searchAllChapters(section) {
        var searchParent;
        var collectionThemes;
        if ((sectionLength) > (section)) {
            searchParent = sectionCollection[section].parentElement;
            searchParent.querySelectorAll('.list__link');
            collectionThemes = searchParent.querySelectorAll('.list__item');
        }
        return collectionThemes;
    }

    // Поиск по колекции глав и возврат колекции со списком тем
    function searchTopics (collectionThemes, head) {
        var nameHead;
        var test;
        var arrTopics = [];
        for (var i = 0; i < collectionThemes.length; i++) {
            nameHead = collectionThemes[i].querySelector('.list__link').textContent;

            if (typeof head === 'number') {
                if (i === head) {
                    var iteration = i;
                    numHead = iteration;

                    test = collectionThemes[iteration -=1].querySelectorAll('.list-sub__link');
                    for (var k = 0; k < test.length; k++) {
                        arrTopics.push(test[k].textContent)
                    }
                }
            } else if (typeof head === 'string') {
                if (nameHead === head) {
                    numHead = +[i] + 1;
                    test = collectionThemes[i].querySelectorAll('.list-sub__link');
                    for (var j = 0; j < test.length; j++) {
                        arrTopics.push(test[j].textContent)
                    }
                }
            }
        }
        return arrTopics;
    }
    function outputStyle(output) {
        var style = 'color: white; font-style: italic; background-color: blue;padding: 2px';

        for (var i = 0; i < arrTopics.length; i++) {
            switch (output) {
                case 'current':
                    console.log(numHead + '.' + count() + ' %c' + arrTopics[i], style);
                    break;
                case 'number':
                    console.log(count() + ' %c' + arrTopics[i], style);
                    break;
                case 'dash':
                default:
                    console.log('— ' + ' %c' + arrTopics[i], style);
                    break;
            }
        }

    }

    outputStyle(output);

}

getData(1, 5, 'current');
