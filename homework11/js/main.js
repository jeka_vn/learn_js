'use strict';
var item = {};
var wrapEl = document.querySelector('#wrap');

wrapEl.onclick = function (e) {
    var eventClass = e.target.className;
    var id = e.target.parentNode.id;
    var itemId = document.querySelector('#' + id);

    switch (eventClass) {
        case 'add-btn':
            var fieldTodoText = document.querySelector('#todoText').value;
            var hash = createHash();
            item[hash] = Todo();
            item[hash].addItem(fieldTodoText, hash);
            break;
        case 'remove-btn':
            item[id].removeItem();
            itemId.remove();
            break;
        case 'edit-btn':
            hiddenElem(id);
            break;
        case 'rename-btn':
            var renameInput = document.querySelector('#renameInput');
            var renameInputBtn = document.querySelector('#renameBtn');
            var renameInputValue = renameInput.value;
            item[id].renameItem(renameInputValue);
            var itemText = itemId.querySelector('#itemText');
            itemText.textContent = item[id].getItem();

            renameInput.remove();
            renameInputBtn.remove();
            itemText.style.display = 'inline-block';

            break;
    }
};
/*
* Функция создание уникального ключа
* return string
* */
function createHash() {
    var date = new Date();
    var hash = date.getTime();
    return 'item' + hash;
}

function Todo() {

    var todoTextVal;

    return {
        addItem: function (val, hash) {
            todoTextVal = val;
            createElement(todoTextVal, hash);
            return todoTextVal;
        },
        removeItem: function () {
            return todoTextVal = '';
        },
        getItem: function () {
            return todoTextVal;
        },
        renameItem: function (newVal) {
            return todoTextVal = newVal;
        }
    };

    function createElement(text, hash) {
        var delBtn = createDelBtn();
        var editBtn = createEditBtn();
        var createSpan = document.createElement('span');
        createSpan.id = 'itemText';
        var createLi = document.createElement('li');
        var idlistEl = document.querySelector('#idlist');
        createSpan.textContent = text;
        createLi.id = hash;
        createLi.appendChild(createSpan);
        createLi.appendChild(editBtn);
        createLi.appendChild(delBtn);
        idlistEl.appendChild(createLi);

    }
    function createDelBtn() {
        var createSpanClose = document.createElement('span');
        createSpanClose.classList.add('remove-btn');
        createSpanClose.id = 'removeBtn';
        createSpanClose.setAttribute('title', 'Удалть елемент');

        return createSpanClose;
    }

    function createEditBtn() {
        var createSpanEdit = document.createElement('span');
        createSpanEdit.classList.add('edit-btn');
        createSpanEdit.id = 'editBtn';
        createSpanEdit.setAttribute('title', 'Изменить елемент(еще не работает. Я в процессе)))');

        return createSpanEdit;
    }
}

function hiddenElem(id) {
    var itemLiId = document.querySelector('#' + id);
    var itemText = itemLiId.querySelector('#itemText');

    var itemTextValue = itemText.textContent;
    itemText.style.display = 'none';

    createInput(id, itemTextValue);
}

function createInput(id, itemTextValue) {

    var btn = document.createElement('button');

    btn.classList.add('rename-btn');
    btn.id = 'renameBtn';
    btn.textContent = 'Изменить';

    var liElem = document.querySelector('#' + id);
    var createInput = document.createElement('input');
    createInput.setAttribute('type', 'text');
    createInput.classList.add('rename-input');
    createInput.id = 'renameInput';
    createInput.value = itemTextValue;
    liElem.appendChild(createInput);
    liElem.appendChild(btn);

    return itemTextValue;
}
