'use strict';
(function () {
    function Trucker() {
        var engineStart = false;
        var credit = 1205;
        var personalMoney = 1861;
        var distanceCovered = 0;
        var allTimeTraveled = 0;
        var tankSize = 2000;
        var amountFuel = 1250;
        var consumption = 40 / 100;
        var fuelPrice = 1.15;

        //Запуск и остановка двигателя
        this.startStop = function () {
            (engineStart === false) ? startStopEngine('start') : startStopEngine('stop');
        };
        this.ride = function (shippingCost, distance) {
            if (engineStart !== false) {
                if (fuelTankEmptying(distance) !== true) {
                    startStopEngine('start');
                    ride(shippingCost);
                    runTruckShow(distance);
                }
                amountOfFuel('start');
            }
        };
        this.initialization = function () {
            routeGeneration();
            showMoneyData();
        };
        // Вызов pop-up
        this.showPopUp = function (flag, instruction) {
            showHidePopUp(flag, instruction);
        };
        this.refueling = function () {
            refueling();
        };
        this.bank = function () {
            bank();
        };

        //	private
        // Функция запуска и остановка двигателя.
        // Принимает один аргумент строкой start/stop {string}.
        function startStopEngine(instruction) {
            var tachometerBox = document.querySelector('#tachometer');
            var arrow_box = tachometerBox.querySelector('.arrow_box');
            var arrow = arrow_box.querySelector('.arrow100w');
            var startStopBtn = document.querySelector('#startStopBtn');

            if (instruction === 'start') {
                engineStart = true;
                startStopBtn.style.opacity = '1';
                arrow.style.transform = 'rotate(11deg)';
                showHideSmoke();
            } else if (instruction === 'stop') {
                engineStart = false;
                startStopBtn.style.opacity = '0.5';
                arrow.style.transform = 'rotate(-37deg)';
            }

            amountOfFuel(instruction);
        }
        /*
        * Функция показа дыма при запуске двигателя и поездке
        * */
        function showHideSmoke() {
            var truckSmokeEk = document.querySelector('#truck_smoke');

            truckSmokeEk.style.opacity = '1';
            setTimeout(function () {
                truckSmokeEk.style.opacity = '0';
            }, 1500);
        }

        /*
        * Функция генирации маршрутов и цен.
        * Записывает згеннерирование маршруты и записывает их в обьект и передает
        * функции creatingAndInsertEl()
         */
        function routeGeneration() {
            var loads = {};
            var city = [
                'Винница', 'Днепр', 'Донецк', 'Житомир', 'Запорожье',
                'Ивано-Франковск', 'Киев', 'Кропивницкий', 'Луганск', 'Луцк',
                'Львов', 'Николаев', 'Одесса', 'Полтава', 'Ровно', 'Симферополь',
                'Сумы', 'Тернополь', 'Ужгород', 'Харьков', 'Херсон', 'Хмельницкий',
                'Черкасы', 'Чернигов', 'Черновцы'
            ];
            city.forEach(function (item) {
                var randonNumber = Math.round(Math.random()*24);
                if (item !== city[randonNumber]) {
                    var route = item + ' - ' + city[randonNumber];
                    loads[route] = randonNumber * 65 + '$';
                }
            });
            creatingAndInsertEl(loads);
        }
        //	Вставка маршрутов в ДОМ
        function creatingAndInsertEl(loadsObj) {
            var burseBox = document.querySelector('#burseBox');
            var crealeEl = document.createElement('li');
            var workItems;

            for (var route in loadsObj) {
                if (loadsObj.hasOwnProperty(route)) {
                    var removeCurrencySign = parseInt(loadsObj[route]);
                    var distance = Math.round(Math.random()*850);
                    (workItems === undefined) ? workItems = htmlEl : workItems += htmlEl;
                    var htmlEl = '<li class="rout" ' +
                        'data-distance="'+ distance +'" ' +
                        'data-price="'+ removeCurrencySign +'">' + route + ' ' + loadsObj[route] + '' +
                        '</li>';
                }
            }
            burseBox.insertAdjacentHTML("afterbegin", workItems);
        }

        //	Вывод информации о финансах
        function showMoneyData() {
            var creditEl = document.querySelector('#credit');
            var personalMoneyEl = document.querySelector('#personalMoney');

            creditEl.textContent = credit + '$';
            personalMoneyEl.textContent = personalMoney + '$';
        }
         /*
         * Функция поездки.
         * Принимает {number} цену перевозки груза.
         * Изменяет положение стрелок спидометра и тахометра и во время поездки
         * скрывает биржу грузов.
         * После поездки добавляю заработаные деньги и
         * обновляю количество заработаных денег функцией showMoneyData()
         * */
        function ride(shippingCost) {
            var speedometer = document.querySelector('#speedometer');
            var speedArrow100w = speedometer.querySelector('.arrow100w');
            speedArrow100w.style.transform = 'rotate(115deg)';

            var tachometer = document.querySelector('#tachometer');
            var tachArrow100w = tachometer.querySelector('.arrow100w');
            tachArrow100w.style.transform = 'rotate(172deg)';

            var burseBox = document.querySelector('#burseBox');
            var burse_container = burseBox.parentNode;
            burse_container.style.opacity = '0';

            setTimeout(function () {
                burse_container.style.opacity = '1';
                speedArrow100w.style.transform = 'rotate(-48deg)';
                tachArrow100w.style.transform = 'rotate(11deg)';
            }, 1500);

            personalMoney += +shippingCost;

            showMoneyData();
        }

        // Вывод пробега авто
        function runTruckShow(distance) {
            var allTimeTraveledEl = document.querySelector('#allTimeTraveled');
            var distanceCoveredEl = document.querySelector('#distanceCovered');

            setTimeout(function () {
                allTimeTraveled += +distance;
                allTimeTraveledEl.textContent = allTimeTraveled;

                distanceCovered = +distance;
                distanceCoveredEl.textContent = distanceCovered;
            }, 1500);

            if (allTimeTraveled >= 99999) allTimeTraveled = 0;


        }

        // опустошение топливного бака
        function fuelTankEmptying(distance) {
            var difference = Math.round(consumption * +distance);

            if (amountFuel <= (tankSize / 4)) {
                showMsg('Бортовой компьютер', 'Осталось мало топлива. ' +
                    'Для поездки в рейс - заправтесь', 'tomato');
                return true;
            } else {
                amountFuel -= difference;
                return false;
            }
        }

        /*
        * Функция показа уровня топлива.
        * Принимает один агргумент {string(start or stop)}
        *
        * */
        function amountOfFuel(instruction) {
            var fuelLevelEl = document.querySelector('#fuelLevel');
            var fuelLevelBox = fuelLevelEl.querySelector('#arrowBox');
            var lowFuelLevel = document.querySelector('#lowFuelLevel');
            var calc = (amountFuel / 11);

            if (instruction === 'start') {
                if (calc >= 145) {
                    fuelLevelBox.style.transform = 'rotate(-140deg)';
                } else if (calc <= 34) {
                    fuelLevelBox.style.transform = 'rotate(-44deg)';
                } else  {
                    fuelLevelBox.style.transform = 'rotate(-' + calc + 'deg)';
                }
            } else if (instruction === 'stop') {
                fuelLevelBox.style.transform = 'rotate(-44deg)';
            }

            if (amountFuel <= (tankSize / 4)) {
                lowFuelLevel.style.opacity = '1';
            } else {
                lowFuelLevel.style.opacity = '0.5';
            }
        }
        /*
        * Функция заправки авто.
        * */
        function refueling() {
            var refuelingInput = document.querySelector('#refuelingInput');
            var difference = (tankSize - amountFuel);
            var refuelingInputVal = +refuelingInput.value;
            var amountForFuel = (refuelingInputVal * fuelPrice);


            if (personalMoney > amountForFuel) {

                if (refuelingInputVal <= difference) {

                    amountFuel += +refuelingInputVal;
                    showHidePopUp('hide', 'del');
                    amountOfFuel('start');
                    showMsg('Заправка', 'Вы заправили в бак ' + refuelingInputVal + 'л.', 'green');
                    personalMoney -= Math.round((refuelingInputVal * fuelPrice));
                    showMoneyData();

                } else if (difference < +refuelingInputVal) {
                    amountFuel += difference;

                    showHidePopUp('hide', 'del');
                    amountOfFuel('start');
                    showMsg('Заправка', 'Удалось заправить ' + difference + 'л. Ваш бак полный', 'orange');
                    personalMoney -= Math.round((difference * fuelPrice));
                    showMoneyData();
                }

            } else {
                showHidePopUp('hide', 'del');
                showMsg('PrivatBank', 'На карте не хватает денег', 'tomato');
            }


        }

        /*
        * Функция показа уведомлений.
        * Принимает заголовок{string}, тело сообщения{string},
        * цвет уведомления {string(green, tomato, red...)}
        * */
        function showMsg(title, text, colorMsg) {

            var msgBox = document.querySelector('#msgBox');
            var msgTitle = msgBox.querySelector('#msgTitle');
            var msgText = msgBox.querySelector('#msgText');

            msgBox.style.background = colorMsg;
            msgTitle.textContent = title;
            msgText.textContent = text;

            msgBox.style.right = '0';

            setTimeout(function () {
                msgBox.style.right = '-320px';
            }, 2500);
        }

        /*
        * Функция открытия/закрытия pop-up.
        * Принимает два аргумента flag{string (show/hide)},
        * instruction{string(refueling, bank, del)}
        * */
        function showHidePopUp(flag, instruction) {
            var popupContainer = document.querySelector('#popupContainer');
            var showCount = incCounter();
            var hideCount = decCounter();

            if (flag === 'show') {
                formCreation('refueling');
                popupContainer.style.display = 'block';
                var show = setInterval(function () {
                    popupContainer.style.display = 'block';
                    popupContainer.style.opacity = '0.' + showCount();

                    if (showCount() === 9) {
                        popupContainer.style.opacity = '1';
                        clearInterval(show);
                    }
                }, 25);
            } else if (flag === 'hide') {
                popupContainer.style.display = 'block';
                var hide = setInterval(function () {
                    popupContainer.style.opacity = '0.' + hideCount();

                    if (hideCount() === 0) {
                        popupContainer.style.opacity = '0';
                        setTimeout(function () {
                            popupContainer.style.display = 'none';
                            formCreation(instruction);

                        }, 250);
                        clearInterval(hide);
                    }
                }, 50);
            }

            // counter
            function incCounter() {
                var count = 0;
                return function () {
                    return count++;
                }
            }
            function decCounter() {
                var count = 9;
                return function () {
                    return count--;
                }
            }
        }

        /*
        * Функция создание формы в зависимости от инструкции.
        * Принимает один аргумент
        * instruction {refueling(создает форму для заправки авто),
        * bank(создает форму для погашения кредита),
        * del(Удаляет созданную форму)}
        * */
        function formCreation(instruction) {
            var popupContainer = document.querySelector('#popupContainer');
            var popup = popupContainer.querySelector('.popup');
            var popUpBox = popup.querySelector('#popUpBox');
            var titlePopUp = popup.querySelector('.title');

            var input = null;
            var btn = null;

            switch (instruction) {
                case 'refueling':
                    titlePopUp.textContent = 'УкрНафта';
                    titlePopUp.setAttribute('title', 'УкрНафта');

                    input = document.createElement('input');
                    input.setAttribute('type', 'number');
                    input.classList.add('refueling_input');
                    input.id = 'refuelingInput';

                    btn = document.createElement('button');
                    btn.classList.add('refueling_btn');
                    btn.id = 'refuelingBtn';
                    btn.textContent = 'Заправить';

                    popUpBox.appendChild(input);
                    popUpBox.appendChild(btn);
                    break;
                case 'bank':
                    titlePopUp.textContent = 'ПриватБанк';
                    titlePopUp.setAttribute('title', 'ПриватБанк');

                    input = document.createElement('input');
                    input.setAttribute('type', 'number');
                    input.classList.add('bank_input');
                    input.id = 'bankInput';

                    btn = document.createElement('button');
                    btn.classList.add('bank_btn');
                    btn.id = 'bankBtn';
                    btn.textContent = 'Погасить';

                    popUpBox.appendChild(input);
                    popUpBox.appendChild(btn);
                    break;
                case 'del':
                    popUpBox.innerHTML = '';
                    break;
            }
        }

        //bank
        function bank() {
            var bankInput = document.querySelector('#bankInput');
            var bankInputVal = +bankInput.value;

            if (bankInputVal < personalMoney) {

                if (bankInputVal > credit) {
                    personalMoney -= credit;
                    showHidePopUp('hide', 'del');
                    showMsg('ПриватБанк', 'Вы погасили '+ credit +'$ кредита', 'green');
                    credit = 0;
                    showMoneyData();
                } else {
                    credit -= bankInputVal;
                    personalMoney -= bankInputVal;
                    console.log(bankInputVal, personalMoney, credit);
                    showMoneyData();
                    showHidePopUp('hide', 'del');
                    showMsg('ПриватБанк', 'Вы погасили '+ bankInputVal +'$ кредита', 'green');
                }

            } else {
                showHidePopUp('hide', 'del');
                showMsg('ПриватБанк', 'Не хватает денег', 'tomato');
            }
        }
    }

    var trucker = new Trucker;

    var wrap = document.querySelector('#wrap');

    wrap.onclick = function (event) {
        var eventClassName = event.target.className;
        console.log(eventClassName);

        switch (eventClassName) {
            case 'start_stop-btn':
                trucker.startStop();
                break;
            case 'rout':
                var shippingCost = event.target.getAttribute('data-price');
                var distance = event.target.getAttribute('data-distance');
                trucker.ride(shippingCost, distance);
                break;
            case 'refueling_container':
                trucker.showPopUp('show', 'refueling');
                break;
            case 'popup_container':
                trucker.showPopUp('hide', 'del');
                break;
            case 'refueling_btn':
                trucker.refueling();
                break;
            case 'bank_container':
                trucker.showPopUp('show', 'bank');
                break;
            case 'bank_btn':
                trucker.bank();
                break;
        }
    };

    window.onload = function () {
        trucker.initialization();
    }
}());
