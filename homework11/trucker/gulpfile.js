'use strict';

var gulp = require('gulp');
var sass = require('gulp-sass');
var rigger = require('gulp-rigger');
var notify = require('gulp-notify');
var browserSync = require('browser-sync').create();
var autoprefixer = require('gulp-autoprefixer');
var csso = require('gulp-csso');
// var concat = require('gulp-concat');
// var uglify = require('gulp-uglify');
// var jsArr = ['src/js/main.js'];

gulp.task('sass', function () {
	return gulp.src('src/scss/style.scss')
		.pipe( sass().on( 'error', notify.onError(
			{
				message: "<%= error.message %>",
				title  : "Ошибка SASS!"
			}))
		)
		.pipe(autoprefixer({
			browsers: ['last 2 versions'],
			cascade: false
		}))
		.pipe(csso())
		.pipe(gulp.dest('build'));
});

gulp.task('html', function (done) {
	gulp.src('src/html/*.html')
		.pipe(rigger())
		.pipe(gulp.dest('build/'));
	done();
});

gulp.task('browser-sync', function() {
	browserSync.init({
		server: {
			baseDir: "build/"
		},
		open: false,
		notify: false
	});
	gulp.watch(["build/**/*.*"]).on('change', browserSync.reload);
});

gulp.task('js', function (done) {
	gulp.src('src/js/main.js')
		.pipe(gulp.dest('build/js/'));
	done();
});

// Move images
gulp.task('move_img', function () {
	gulp.src('src/img/**/*.*')
	// .pipe(imagemin())
		.pipe(gulp.dest('build/img/'))
});


gulp.task('watch', function() {
	gulp.watch('src/scss/**/*.scss', gulp.series('sass'));
	gulp.watch('src/html/**/*.html', gulp.series('html'));
	gulp.watch('src/js/*.js', gulp.series('js'));
});
gulp.task('default', gulp.series(
	gulp.parallel('watch', 'browser-sync', 'move_img')
));
