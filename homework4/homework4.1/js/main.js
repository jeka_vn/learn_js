'use strict';
var userData = +prompt('Введите число');
var resultValidate = validate(userData);
var resultSqrt = getSquareHorse(resultValidate);

showResult(resultSqrt);

function showResult(endHum) {
	for (var i = 1; i <= endHum; i++) {
		console.log(i);
		if (i >= 100) break;
	}
}

function getSquareHorse(num) {
	return Math.sqrt(num);
}

function validate(data) {
	if (!isNaN(data)) {
		if (data !== 0) {
			return data;
		} else {
			errorMsg('Вы ввели ноль или меньше.');
		}
	} else {
		errorMsg('Вы ввели не число.');
	}
}

function errorMsg (msg) {
	console.log(msg);
}