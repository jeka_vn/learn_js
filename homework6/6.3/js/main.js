'use sctrict';

var products = [
{
	'name': 'lada',
	'price': 12000,
	'category': 'Транспорт'
},
{
	'name': 'iveco',
	'price': 102000,
	'category': 'Транспорт'
},
{
	'name': 'Xiaomi',
	'price': 4500,
	'category': 'Телефоны'
},
{
	'name': 'Nokia',
	'price': 9500,
	'category': 'Телефоны'
},
{
	'name': 'iphone',
	'price': 19500,
	'category': 'Телефоны'
},
{
	'name': 'oppo',
	'price': 19500,
	'category': 'Телефоны'
},
{
	'name': 'man',
	'price': 102000,
	'category': 'Транспорт'
},
{
	'name': 'Пицца',
	'price': 90,
	'category': 'Еда'
},
{
	'name': 'Саллат',
	'price': 115,
	'category': 'Еда'
},
{
	'name': 'Хот-дог',
	'price': 40,
	'category': 'Еда'
}
];
var result = [];
var showInputResult = true;

// Вызаваем функцию showInput(), пока переменная showInputResult = true
// while (showInputResult) {
// 	showInput();
// }

// Функция вывлодит prompt и справшивает хочет ли пользователь продолжить ввод
// и если нет - приваеваем переменой showInputResult = false иначе true
function showInput() {
	var nameProduct = prompt('Введите название продукта');
	var productPrice = +prompt('Ведите цену товара'); // Могу текст ввести. И там NaN.
	var productCategory = prompt('Введите категорию товара');
	var popupConfirm = confirm('Хотите добаить продукт?');

	if (popupConfirm === false) {
		showInputResult = false;
	} else {
		showInputResult = true;
	}
	readData(nameProduct, productPrice, productCategory);
}

// Функция принимает три аргумента: название товара, цену и категорию,
// после записывает их в оььект productObj и методом push() добавляет новый
// обьект в массив products.
function readData(name, price, cat) { // Почему "readData", если пишет?
	var productObj = {
		'name': name,
		'price': price,
		'category': cat
	};

	products.push(productObj);
}


// SortPrice
function sortPrice(priceStart, priceEnd, sortType) {
	var arrProductSortPrice = [];
	var objProductSortPrice;
	result = [];

	products.forEach( function(obj, item) {
		if ((obj['price'] >= priceStart) && (obj['price'] <= priceEnd)) {
			arrProductSortPrice.push(obj);
		}
	});

	arrProductSortPrice.forEach( function(obj, item) {
		 objProductSortPrice = {
			'name': obj.name,
			'price': obj.price,
			'category': obj.category
		};

		result.push(objProductSortPrice);

		sortOption(sortType, result);

	});
	return result;
}
/*
Функция принимает тип сортировки и массив сортировки и взависимости от
вида сотрировки сортирует по убыванию цены или по возростанию и позвращает массив
*/
function sortOption(sortType, arrProduct) {
	arrProduct.sort(function(a, b) {
		if (sortType === 'asc') {
			return a.price - b.price;
		} else if (sortType === 'desc') {
			return b.price - a.price;
		}
	});
	return arrProduct;
}


// SortCategty
/*
Функция принимает название категории и возвращает массив с продуктами
указаной категории
*/
function sortCat(cat) {
	var arrProductSortCat = [];
	var objProductSortCat;
	result = [];

	products.forEach( function(obj, item) {
		if (obj['category'] === cat) {
			arrProductSortCat.push(obj);
		}
	});

	arrProductSortCat.forEach( function(obj, item) {
		var objProductSortCat = {
			'name': obj.name,
			'price': obj.price,
			'category': obj.category
		};
		result.push(objProductSortCat);
	});
	return result;
};

// AmountProduct
/*
Функция подсчета продуктов определенной категории.
Принимает название категории, передает ее функции sortCat() и возвращает длину массива
*/
function amountProduct(cat) {
	result = [];
	sortCat(cat);
	return result.length;
}

// Delete products
/*
Функция принимает название продукта, проходится по массиву, ищет совпадение,
удаляет его и возвращает массив
*/
function removeProducts(nameProduct) {

	products.forEach( function(item, i) {
		if (item['name'] === nameProduct) {
			products.splice(i, 1);
		}
	});
	return products;
}

/*
Функция принимает четыре аргумента: тип сортировки, нижний порог цены, верхний
порог цены, категория(не обязательно) и возвращает массив. Если указана и категория, то 
вернется массив с указаной катерорией
*/
function userFilter(sortType, startPrice, endPrice, cat) {
	result = [];

	sortPrice(startPrice, endPrice, sortType);

	if (cat !== undefined) sortCat(cat);

	sortOption(sortType, result);

	return result;
}

// Не нашёл возврат суммы товаров.

