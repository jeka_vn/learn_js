'use strict';

var userData = prompt('Введите числа через запятую');

if (userData !== null) {
	var arr  = userData.split(', '); // разбиваем строку на массив
	validate(arr); // передаем массив на проверку
} else {
	showMsg('Вы отменили ввод');
}


function showMsg(msg) {
	console.log(msg);
}

// Функция принимает массив, проверяет в цыкле не являеться ли каждий елемент массива
// меньше 3-х, больше 10-и или сумма елемента не больше 100.
// Если елемент не проходит проверку - передаем сообщение через функцию showMsg
// и переменной flag присваиваем false
// Если flag не false передаем массив функции sort()
function validate(arr) {
	var flag = true;

	for (i = 0; i < arr.length; i++) { // Uncaught ReferenceError: i is not defined

		if (arr[i].length < 3) {
			showMsg('Меньше 3');
			flag = false;
		} else if (arr[i].length > 10) {
			showMsg('Больше 10');
			flag = false;
		} 
		if (calc(arr[i]) > 100) {
			showMsg('Больше 100');
			flag = false;
		}

	}

	if (flag !== false) { sort(arr); }

}


function calc(item) {

	var arr = item.split('');

	var result = 0;

	arr.forEach( function(item) {
		(result === 0) ? result = +item : result += +item;
	});

	return result;
}


function sort(arr) {
	var newArr = [];
	var evenNum;
	var oddNum;

	arr.forEach( function(item) {
		for (var i = 0; i < item.length; i++) {
			newArr.push(item[i]); // тут ошибка
		}
	});


	newArr.sort(function(a, b) {
		return b - a;
	});
	var desc = newArr.join();
	showMsg(desc); // Выводит "8,8,7,7,7,7,6,6,6,6,5,5,5,5,5,5,5,5,4,4,4,4,4,3,3,3,2,2,2". Должно числа сортировать.

	for (var i = 0; i < newArr.length; i++) {
		if (newArr[i] % 2 === 0) { // Верно, попробуйте ещё с встроенной функцией sort.
			(evenNum === undefined) ? evenNum = newArr[i] : evenNum += newArr[i];
			evenNum += newArr[i];
		} else if (newArr[i] % 2 !== 0) {
			(oddNum === undefined) ? oddNum = newArr[i] : oddNum += newArr[i];
		}
	}
	showMsg(evenNum); // Выводит "8888666666664444444444222222"
	showMsg(oddNum); // Выводит "777755555555333"
}

// 57638725, 2354, 54467, 556, 784, 235465