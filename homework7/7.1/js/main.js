'use sctrict';

var level = {
  1: 3000,
  2: 2500,
  3: 2000,
  4: 1500,
  5: 1000,
};


var container = document.getElementById('containerGame');
var canvasItem = document.getElementById('canvas_item');
var amountElem = document.getElementById('timeGame');
var timeText = document.getElementById('numHits');
var classEvent;
var gameLevel = 1;
var amount = 0;
var playTime = 10;
var gameStarted = false;

getResult();

container.onclick = function(e) {
  classEvent = e.target;

  switch (classEvent.className) {
    case 'level':
      var valElem = classEvent.getAttribute('lvl');
      levelCheck(valElem);
      break;
    case 'item_images':
      canvasItem.setAttribute('src', classEvent.src);
      nameChange(classEvent.id);
      break;
    case 'canvas_item':
      startGame(gameLevel, playTime);
      break;
  }
};
// функция получает два аргумента. Первый уровень игры, второй время игры.
// Присваивает переменной gameStarted = true
// Запускает функцию imgRandomPosition() котрая рандомно изменяет положение
// апельсина/мантарина на холсте

// Функция clickAmount() ведет подсчет кликов по апельсину/мантарину
// Таймер вызывает функцию imgRandomPosition() через определенное время(в зависимости от выбраного уровня игры)
// Обнуляем setInterval .
function startGame(gameLevel, playTime) {
  gameStarted = true;
  imgRandomPosition();
  clickAmount();

  let timer = setInterval(function() {
    imgRandomPosition();
  }, level[gameLevel]);

  setTimeout(function() {
    clearTimeout(timer);
  }, playTime * 1000);
}

// Функция принимает один аргумент - уровень игры.
// Проверяет не началась ли игра и если нет , проверяет есть ли уровень выбраний
// пользователем в обьекте level и если есть - записываем уровень игры в переменную gameLevel.
//
function levelCheck(numLevel) {
  var levelBox = document.querySelector('#levelBox');
  var levelColection = levelBox.querySelectorAll('.level');
  var thisKey;
  if (gameStarted === false) {
    for (key in level) {
      if (key === numLevel) {
        gameLevel = key;
        thisKey = +key;
        levelColection[thisKey -1].classList.add('active');
      }
    }

    for (var i = 0; i < levelColection.length; i++) {
      if (i !== (thisKey -1)) {
        levelColection[i].classList.remove('active');
      }
    }

  }
}
// Функция ведет обратный отсчет и если время игры еще не вышло - показывает
// пользователю сколлько времени еще осталось.

// Если премя игры вышло, то уведомляет пользователя что время вышло.
// Функция hideCanvasItem() скрывает апельсин/мандарин.
// Функция setResult() записывает в localstorage уровень игры и количество попаданий
// Функция getResult() выводит пользивателю его последний результат
// Присваевием переменной gameStarted = false
function timer() {
  let startTime = new Date().getTime();
  let time = 10;


  var timer = setInterval(function() {
    let currentTime = new Date().getTime();
    let difference = currentTime - startTime;
    let seconds = Math.round(difference / 1000);

    if (seconds === playTime) {
      timeText.innerText = 'Время вышло.';
      hideCanvasItem();
      clearInterval(timer);
      setResult(gameLevel, amount);
      getResult();
      gameStarted = false;
    } else {
      timeText.innerText = 'Осталось ' + (playTime - seconds) + ' секунд';
    }
  }, 1000);
}

// Функция запускает таймер если переменная равна 0.
// При каждом клике на апельсин/мантарин увеличивает переменную amount и
// сообшает пользовател сколько раз он попал на дванный момент.
function clickAmount() {
  if (amount === 0) timer();

  amount++;
  amountElem.innerText = 'Вы попали ' + amount + ' раз';
}

// Функция рандомного положения апельсина/мандарина
function imgRandomPosition() {
  let top = Math.random() * 90;
  let left = Math.random() * 90;

  canvasItem.style.top = top + '%';
  canvasItem.style.left = left + '%';
}

// Функция скривает апельсин/мандарин
function hideCanvasItem() {
  canvasItem.style.display = 'none';
}

// Функция менает название страницы и название игры в зависимости от того, чем
// пользователь играет
function nameChange(idImg) {
  let titleElem = document.getElementById('gameTitle');
  let nameGame = {
    'orange': 'Апельсинмания',
    'mandarin': 'Мандаринмания',
  };

  for (key in nameGame) {
    if (key === idImg) {
      titleElem.innerText = nameGame[key].toUpperCase();
      document.title = nameGame[key].toUpperCase();
      break;
    }
  }
}


// Функция принимает два аргумента уровень игры и количество попададний и
// записывет результат в localstorage
function setResult(gameLevel, amount) {
  let result = {
    'level': gameLevel,
    'amount': amount,
  };

  localStorage.setItem('result', JSON.stringify(result));
}


function getResult() {
  let result = localStorage.getItem('result');
  result = JSON.parse(result);
  let noResult = document.getElementById('noResult');
  let resultBox = document.getElementById('resultBox');

  if (result != null) {
    let resultText = 'Уровень: <b>' + result.level + '</b> Результат: <b>' + result.amount + '</b>';
    let createLiElem = '<li>' + resultText + '</li>';
    if (noResult != null) noResult.remove();
    resultBox.innerHTML = createLiElem;
  }
}

